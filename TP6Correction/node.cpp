#include "node.h"

/**
 * @brief node::node create a node
 * @param l the letter in the node
 * @param f true if the letter is the last of a word
 */
node::node(char l, bool f):letter(l),final(f),child(nullptr),sibling(nullptr)
{
}

node::~node(){
    delete sibling;
    delete child;
}

/**
 * @brief node::add add a word to the dictionary
 * @param word the word to add
 * @param level the position of the letter in the word (0=first)
 */
void node::add(std::string word, int level)
{
    bool fin = false;
    char c = word[level];

    if(level == word.length()-1)
    {
        fin = true;
    }

    if(c == letter)
    {
        if(fin)
        {
            this->final = true;
        }
        else
        {
            if(this->child == nullptr)
            {
                this->child = new node(word[level+1],false);
            }
            this->child->add(word,level+1);
        }
    }
    else
    {
        if(this->sibling == nullptr)
        {
            this->sibling = new node(c,fin);
        }
        this->sibling->add(word,level);
    }
}

/**
 * @brief node::find search for the word
 * @param word the word to find
 * @param level the letter position
 * @return true if the word is found into the node or its children
 */
bool node::find(std::string word, int level) const
{
    bool ok=false;

    bool fin = false;
    char c = word[level];

    if(level == word.length()-1)
    {
        fin = true;
    }

    if(c == letter)
    {
        if(fin)
        {
            ok = true;
        }
        else
        {
            if(this->child == nullptr)
            {
                ok = false;
            }
            else
            {
                ok = this->child->find(word,level+1);
            }
        }
    }
    else
    {
        if(this->sibling == nullptr)
        {
            ok = false;
        }
        else
        {
            ok = this->sibling->find(word,level);
        }
    }

    return ok;
}

/**
 * @brief node::complete do the completion
 * @param partial the partial word to complete
 * @param words (out) array of strings, the complete words
 * @param level node level
 */
void node::complete(std::string partial, std::vector<std::string> &words, int level) const
{    
    if(level < partial.length())
    {
        if(partial[level] != this->letter)
        {
            if(this->sibling != nullptr)
            {
                this->sibling->complete(partial,words,level);
            }
        }
        else
        {
            if(this->child != nullptr)
            {
                this->child->complete(partial,words,level+1);
            }
        }
    }
    else
    {
        if(this->sibling != nullptr)
        {
            this->sibling->complete(partial,words,level);
        }
        partial += letter;

        if(final)
        {
            words.push_back(partial);
        }
        if(this->child != nullptr)
        {
            this->child->complete(partial,words,level+1);
        }
    }
}
