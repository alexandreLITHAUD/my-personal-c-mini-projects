#include "dns.h"
#include <string>

/**
 * @brief Constructeur du dns (vide)
 */
dns::dns()
{
}

/**
 * @brief procédure permettant d'ajouter une valeur a la Map
 * @param l'url du dns
 * @param l'ip du dns
 */
void dns::add(std::string s, IP ip)
{
    this->carte[s] = ip;
}

/**
 * @brief procédure permettant d'enlever une valeur a la Map
 * @param l'url à enlever de la liste
 */
void dns::remove(std::string s)
{
    this->carte.erase(s);
}

/**
 * @brief fonction permettant de trouver l'ip d'une url
 * de la Map
 * @param l'url dont on cherche l'ip
 * @return l'ip de l'url
 */
IP dns::find(std::string s)
{
    IP ip = NULL;

    if(this->carte.count(s))
    {
        auto i = this->carte.find(s);
        ip = i->second;
    }

    return ip;
}

/**
 * @brief fonction permettant de trouver si l'url existe
 * dans la Map
 * @param l'url a chercher
 * @return True si l'url existe dans la Map
 */
bool dns::exists(std::string s)
{
    bool res = false;

    if(this->carte.count(s))
    {
        res = true;
    }
    return res;
}

/**
 * @brief fonction permettant de retouner la liste de
 * tous les urls et Ip de la Map
 * @return la chaine contenant toutes les information de la Map
 */
std::string dns::to_string() const
{
    std::string str = "";
    for(std::pair<std::string, IP> element : this->carte)
    {
        str += element.first + ":" + element.second.to_string() + "\n";
    }
    return str;
}
