#ifndef DNS_H
#define DNS_H
#include <iostream>
#include <map>
#include <string>
#include "ip.h"

/**
 * @brief The dns class
 * contains a map that keep the urls and Ip
 */
class dns
{
private:
    std::map<std::string,IP> carte;
public:
    dns();
    IP find(std::string s);
    bool exists(std::string s);
    void remove(std::string s);
    void add(std::string, IP ip);
    std::string to_string() const;
};

#endif // DNS_H
