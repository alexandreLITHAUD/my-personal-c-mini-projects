#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <numeric>
#include <vector>
#include <list>
#include <chrono>

using namespace std;


int main()
{
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();

    vector<float> tab;

    for(int i=0; i<100; i++)
    {
        tab.push_back(std::rand());
    }

    for(unsigned long j=0; j<tab.size();j++){
        cout << tab[j] << endl;
    }

    cout << "C'est la fin du tableaux !" << endl;

    std::sort(tab.begin(),tab.end());

    for(float val:tab){
       cout << val << endl;
    }

    cout << "C'est la fin du tableaux trie !" << endl;

    std::random_shuffle(tab.begin(),tab.end());

    std::for_each(tab.begin(),tab.end(),[](float i) {cout << i << endl;} );

    cout << "C'est la fin du tableaux remelange !" << endl;

    std::for_each(tab.rbegin(),tab.rend(),[](float i) {cout << i << endl;});

    cout << "C'est la debut du tableaux remelange !" << endl;


    float temp = std::accumulate(tab.begin(),tab.end(),0);
    cout << "la taille totale est de " << temp << endl;

    cout << "le plus petit element est " << *std::min_element(tab.begin(),tab.end()) << endl;

    cout << "le plus grand element est " << *std::max_element(tab.begin(),tab.end()) << endl;
}
