#include "node.h"

//---------------------- PARTIE A COMPLETER ----------------

/**
 * @brief Node::Add add a value to the node
 * @param v the value to add
 */
void Node::Add(int v)
{
    if(v<this->value)
    {
        if(this->left == nullptr)
        {
            this->left = new Node(v);
            this->left->parent = this;
        }
        else
        {
             this->left->Add(v);
        }
    }

    else if(v>this->value)
    {
        if(this->right == nullptr)
        {
            this->right = new Node(v);
            this->right->parent = this;
        }
        else
        {
             this->right->Add(v);
        }
    }
}

/**
 * @brief Node::Find search a value in the node (and its children)
 * @param v the value to find
 * @return true if the value is founded
 */
bool Node::Find(int v) const
{
    bool res = false;

    if(v<this->value)
    {
        if(this->left == nullptr)
        {
            res = false;
        }
        else
        {
            res = this->left->Find(v);
        }
    }
    else if(v>this->value)
    {
        if(this->right == nullptr)
        {
            res = false;
        }
        else
        {
             res = this->right->Find(v);
        }
    }
    else
    {
        res = true;
    }

    return res;
}

/**
 * @brief Node::Size Gives the size of the subtree
 * @return the number of nodes
 */
int Node::Size() const
{
    int nbr = 1;

    if(this->left != nullptr)
    {
        nbr += this->left->Size();
    }

    if(this->right != nullptr)
    {
        nbr += this->right->Size();
    }

    return nbr;

}




//-------------- FIN DE LA PARTIE A COMPLETER ---------------

/**
 * @brief Node::Node create a node
 * @param v the value inside the node
 */
Node::Node(int v, Node* papa)
    : value(v), left(nullptr), right(nullptr), parent(papa)
{

}

Node::~Node()
{
    delete left;
    delete right;
}


/**
 * @brief Node::Get read the value of the node
 * @return the node's value
 */
int Node::Get() const
{
    return value;
}
