#include "tree.h"
#include "node.h"

//---------------- PARTIE A COMPLETER ---------------------------
/**
 * @brief Tree::Begin gives iterator on the beginning of the tree
 * @return iterator on the first item
 */
TreeIterator Tree::Begin()
{
    Node* n = this->root;
    if(n != nullptr)
    {
        while(n->Left() != nullptr)
        {
            n = n->Left();
        }
    }

    auto t = new TreeIterator(n);
    return *t;
}

//------------ FIN DE LA PARTIE A COMPLETER -----------------------

/**
 * @brief Tree::Tree create a binary search tree
 */
Tree::Tree()
    : root(nullptr)
{

}

Tree::~Tree()
{
    delete root;
}

/**
 * @brief Tree::Add add a value in the tree
 * @param v value to add
 */
void Tree::Add(int v)
{
    if(root)
        root->Add(v);
    else
        root = new Node(v);
}

/**
 * @brief Tree::Find search a value
 * @param v the value to search
 * @return true if the value is inside the  tree
 */
bool Tree::Find(int v) const
{
    bool ok=false;
    if(root)
        ok = root->Find(v);
    return ok;
}

/**
 * @brief Tree::Size size of the tree
 * @return number of values (nodes)
 */
int Tree::Size() const
{
    int size=0;
    if(root)
        size += root->Size();
    return size;
}

/**
 * @brief Tree::Get value of the root node
 * @return the value of the root node
 * @throws if tree is empty (no root node)
 */
int Tree::Get() const
{
    if(root==nullptr) throw "erreur";
    return root->Get();
}



/**
 * @brief Tree::End gives an invalid iterator
 * @return invalid iterator (above the end of the tree)
 */
TreeIterator Tree::End()
{
    return TreeIterator(nullptr);
}
