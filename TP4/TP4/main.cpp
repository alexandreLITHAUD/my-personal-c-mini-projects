
#include <iostream>
#include <cstdlib>
#include "tree.h"
#include "treeiterator.h"

using namespace std;

int main()
{    
    Tree tree;
//*first part ----------------------------------
    for(int i=0;i<100;i++)
    {
        tree.Add(rand()%100);
    }

    cout << "Binary search tree :  "<<tree.Size()<<" nodes";

    int test=0;
    while(test>=0)
    {
        cout << endl << "Test : (<0 for stop) ";
        cin >> test;
        if(tree.Find(test))
            cout << "is in !";
        else
            cout << "is not in !";
    }
//-------------------------------- first part*/

///* second part --------------------------------------
    cout << endl << "Test of infix path"<<endl;
    for(TreeIterator i=tree.Begin();!i.IsEqual(tree.End());i.Next())
    {
        cout << i.Get() << "-" ;
    }
//------------------- second part */
    return 0;
}

