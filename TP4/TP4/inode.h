#ifndef INODE_H
#define INODE_H

/**
 * @brief The INode class Interface for a binary search tree
 */
class INode
{
public:
    virtual ~INode(){}
	/**
	* Add a value
	* @param v the value to add
	*/
    virtual void Add(int v)=0;
	/**
	* Search a value
	* @param v the value to search
	* @return true if the value if founded
	*/
    virtual bool Find(int v) const = 0;
	/**
	* Computes the size of the tree
	* @return the number of values
	*/
    virtual int Size() const = 0;
	/**
	* Read the value of the node
	* @return the value of the node
	*/
    virtual int Get() const = 0;
};

#endif // INODE_H

