#include <cassert>
#include "treeiterator.h"

//--------------- PARTIE A COMPLETER ------------------
/**
 * @brief TreeIterator::Next go the next item
 * @pre iterator must me valid (node not null)
 */
void TreeIterator::Next()
{
    assert(node); // ne pas retirer

    if(this->node == nullptr)
    {

    }
    else if(this->node != nullptr)
    {

        if(this->node->Right() != nullptr)
        {
            this->node = this->node->Right();
            while(this->node->Left() != nullptr)
            {
                this->node = this->node->Left();
            }
        }

        else if(this->node->Right() == nullptr)
        {
            int val = this->node->Get();
            while (this->node != nullptr and this->node->Get()<=val)
            {
                this->node = node->Parent();
            }
        }
    }

}


//--------------- FIN DE LA PARTIE A COMPLETER ---------

/**
 * @brief TreeIterator::TreeIterator Create the iterator
 * @param n the node inside the iterator
 */
TreeIterator::TreeIterator(Node *n)
 : node(n)
{

}

/**
 * @brief TreeIterator::Get
 * @return the value referenced by the iterator
 * @pre the iterator must be valid 
 */
int TreeIterator::Get() const
{
    assert(node);
    return node->Get();
}

/**
 * @brief TreeIterator::IsEqual compare two iterators
 * @param i the other iterator
 * @return true if both iterators referenced the same node
 */
bool TreeIterator::IsEqual(TreeIterator i) const
{
    return node == i.node;
}


