#ifndef TREE_H
#define TREE_H

#include "inode.h"
#include "treeiterator.h"

/**
 * @brief The Tree class binary search tree
 * compose nodes
 * @see Node
 */
class Tree : INode
{
private:
    Node* root;
public:
    Tree();
    ~Tree();
    void Add(int v);
    bool Find(int v) const;
    int Size() const;
    int Get() const;
    TreeIterator Begin() ;
    TreeIterator End() ;
};

#endif // TREE_H
