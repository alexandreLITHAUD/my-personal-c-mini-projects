#ifndef TREEITERATOR_H
#define TREEITERATOR_H

#include "node.h"

/**
 * @brief The TreeIterator class binary search tree iterator
 * associated with a node
 * @see Node
 */
class TreeIterator
{
private:
    Node* node;
public:
    TreeIterator(Node* node=nullptr);
    int Get() const;
    void Next();
    bool IsEqual(TreeIterator i) const;
};

#endif // TREEITERATOR_H
