#ifndef NODE_H
#define NODE_H

#include "inode.h"

/**
 * @brief The Node class : Node of a binary search tree
 */
class Node : public INode
{
private:
    int value;
    Node* left;
    Node* right;
    Node* parent;
public:
    Node(int v, Node *dad=nullptr);
    ~Node();
    Node* Left() const {return left;}
    Node* Right() const {return right;}
    Node* Parent() const {return parent;}
    void Add(int v);
    bool Find(int v) const;
    int Size() const;
    int Get() const;
};

#endif // NODE_H
