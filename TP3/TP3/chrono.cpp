﻿//---------------------------------------------------------------------------
#include "chrono.h"
//---------------------------------------------------------------------------

namespace iut
{	 
/**
* start the timer
*/
	void Chrono::start()
	{
#ifdef _WIN32
        ::QueryPerformanceCounter(&clock1);
#else
        startTime = std::chrono::high_resolution_clock::now();
#endif
	}

/**
* stop the timer
*/
	void Chrono::stop()
	{
#ifdef _WIN32
        ::QueryPerformanceCounter(&clock2);
#else
        stopTime = std::chrono::high_resolution_clock::now();
#endif
	}


/**
* @return the time, in milliseconds, of the time elapsed
*/
	double Chrono::totalTime() const
	{
#ifdef _WIN32
        LARGE_INTEGER freq;
		::QueryPerformanceFrequency(&freq);

        return 1000.0*(double)((clock2.QuadPart - clock1.QuadPart))/(double)freq.QuadPart ;
#else
        return std::chrono::nanoseconds(stopTime - startTime).count() / 1000.0;
#endif
	}
}

