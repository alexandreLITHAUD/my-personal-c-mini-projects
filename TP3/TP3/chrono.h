﻿//---------------------------------------------------------------------------

#ifndef chronoH
#define chronoH
//---------------------------------------------------------------------------
#ifdef _WIN32
#include <windows.h>
#else
#include <chrono>
#endif
namespace iut
{
	/**
	* A precise count timer
    * The Win32 SDK is used for windows, another for other OS
	* @author aguidet
    * @version 2.0
	*/
    class Chrono
	{
		private:
#ifdef _WIN32
			LARGE_INTEGER clock1;
			LARGE_INTEGER clock2;
#else
            std::chrono::system_clock::time_point startTime;
            std::chrono::system_clock::time_point stopTime;
#endif
		public:
            void start();
            void stop();			
            double totalTime() const;
	};

	/**
	* Class to automate the timer calculation
	* @author aguidet
	* @version 1.0
	*/
	class RAIIChrono
	{
		private:
			double& duree;
			Chrono c;
		public:
            RAIIChrono(double& d):duree(d){c.start();}
            ~RAIIChrono(){c.stop(); duree = c.totalTime();}
	};
}


#endif
