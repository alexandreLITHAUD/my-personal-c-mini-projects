TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        chrono.cpp \
        hash_str.cpp \
        main.cpp

HEADERS += \
    chrono.h \
    hash_str.h
