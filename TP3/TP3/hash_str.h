#ifndef HASH_STR_H
#define HASH_STR_H

#include <string>
#include <vector>
#include <list>

/**
 * @brief La classe qui créer une table de hachage
 * @author Alexandre Lithaud
 */
class hash_str{
    std::vector<std::list<std::string>>table;
    int hash(std::string s)const;
public:
    hash_str(int size=100);
    void add(std::string s);
    bool contains(std::string s)const;
    void remove(std::string s);
};

#endif // HASH_STR_H
