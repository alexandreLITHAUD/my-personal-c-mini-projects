#include "ip.h"
#include <sstream>
#include <algorithm>

/**
* @brief Create an IP address
* @param s the address coded as a string like "a.b.c.d"
*/
IP::IP(std::string s)
{
    std::replace(s.begin(),s.end(), '.',' ');
    std::istringstream flux(s);
    unsigned int a,b,c,d;
    flux >> a>>b>>c>>d;
    ipV4 = (a<<24)|(b<<16)|(c<<8)|d;
}

/**
* @brief Convert an ip address in string
* @return string formated as "a.b.c.d"
*/
std::string IP::to_string() const
{
    std::ostringstream flux;
    flux << (int)octets[3]<<'.'<<(int)octets[2]<<'.'<<(int)octets[1]<<'.'<<(int)octets[0];
    return flux.str();
}
