TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        dns.cpp \
        ip.cpp \
        main.cpp

HEADERS += \
    dns.h \
    ip.h
