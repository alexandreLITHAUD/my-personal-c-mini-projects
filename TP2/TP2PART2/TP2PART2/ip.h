#ifndef IP_H
#define IP_H
#include <string>
/**
* @brief an IPV4 address
* the IPV4 address is 32 bits long
* it can be considered as a single unsigned integer, or 4 unsigned bytes
*/
union IP
{
private:
    uint8_t octets[4];
    unsigned int ipV4;
public:
    IP(unsigned int ip=0):ipV4(ip){}
    IP(std::string s);
    std::string to_string() const;
	/**
	* @brief get the 32 bits-value 
	* @return the IP address as a 32-bits value
	*/
    unsigned int value() const {return ipV4;}
};



#endif // IP_H
