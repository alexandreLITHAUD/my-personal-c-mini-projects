#include <iostream>
#include "dns.h"
#include "ip.h"

using namespace std;

int main()
{
    cout <<"Exercice2-dictionnaires" <<endl;
    dns dns;
    dns.add("www.google.fr",IP("66.102.1.94"));
    dns.add("www.microsoft.fr",IP("65.52.144.160"));
    dns.add("www.apple.fr",IP("17.172.224.111"));
    dns.add("www.ubuntu.fr",IP("217.70.184.50"));
    dns.add("www.debian.fr",IP("94.143.220.132"));

    cout<<dns.to_string()<<endl;
    cout<<dns.exists("www.google.fr")<<endl;
    cout<<dns.exists("www.facebook.com")<<endl;
    cout<<dns.find("www.microsoft.fr").to_string()<<endl;
    cout<<dns.find("www.facebook.com").to_string()<<endl;
    dns.remove("www.facebook.com");
    dns.remove("www.apple.fr");
    cout<<dns.to_string()<<endl;
}
