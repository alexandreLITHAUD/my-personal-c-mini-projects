#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <numeric>
#include <vector>
#include <list>
#include <chrono>

using namespace std;


int main()
{

    list<float> listVal;

    for(int i=0;i<100;i++)
    {
        listVal.push_back(std::rand());
    }

    std::list<float>::iterator it = listVal.begin();
    for(unsigned long j=0; j<listVal.size();j++)
    {
        cout << *it << endl;
        std::advance(it,1);
    }

    cout << "fin du tableau !" << endl;

    listVal.sort();

    for(float f:listVal)
    {
        cout << f << endl;
    }

    cout << "fin du tableau trie !" << endl;


    //std::random_shuffle(listVal.begin(),listVal.end()); IMPOSSIBLE

    std::for_each(listVal.begin(),listVal.end(), [](float i) {cout << i << endl;});

    cout << "On ne peux pas melager donc voici la fin du tableau trie !" << endl;

    std::for_each(listVal.rbegin(),listVal.rend(), [](float i) {cout << i << endl;});

    cout << "On ne peux pas melager donc voici le DEBUT du tableau trie !" << endl;

    float temp = std::accumulate(listVal.begin(),listVal.end(),0);

    cout << "le total de toues les valeurs est " << temp << endl;

    cout << "la plus petite valeur est " << *std::min_element(listVal.begin(),listVal.end()) << endl;

    cout << "la plus grande valeur est " << *std::max_element(listVal.begin(),listVal.end()) << endl;

}
