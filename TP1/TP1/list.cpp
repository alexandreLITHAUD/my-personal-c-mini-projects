#include "list.h"

/**
 * TODO
 * @brief List::List init the list
 */
List::List()    
{
    this->first = nullptr;
}


/**
 * TODO
 * @brief List::~List release the memory
 */
List::~List()
{
    delete this->first;
}

/**
 * TODO
 * @brief List::AddFirst Add a value at the beginning of the list
 * @param val the value to add
 */
void List::AddFirst(int val)
{
    if(this->first == nullptr)
    {
        this->first = new Element(val);
    }
    else
    {
        Element* temp = this->first;
        this->first = new Element(val);
        this->first->Connect(temp);
    }
}

/**
 * TODO
 * @brief List::AddLast add a value at the end of the list
 * @param val the value to add
 */
void List::AddLast(int val)
{
    if(this->first == nullptr)
    {
        this->AddFirst(val);
    }
    else
    {
        this->first->AddLast(new Element(val));
    }

}

/**
 * TODO
 * @brief List::Find Search the value inside the list
 * @param val the value to find
 * @return true if the val is inside the list
 */
bool List::Find(int val) const
{
    bool ok=false;
    ok = this->first->Find(val);
    
    return ok;
}

/**
 * TODO
 * @brief List::Size calculates the number of values in the list
 * @return the number of list elements
 */
int List::Size() const
{
    int size=1;
    size = this->first->Size();
    
    return size;
}

/**
 * TODO
 * @brief List::Get gives the i-th value
 * @param i the desired range ([0..size[)
 * @return the value
 * @throws an char* message if bad range
 */
int List::Get(int i) const
{
    Element* e = this->first;

    for(int val=0;val<i;val++)
    {
        if(e->GetNext() == nullptr)
        {
            return 0;
        }
        e = e->GetNext();
    }

    return e->GetValue();
}

/**
 * TODO
 * @brief List::RemoveLast Removes the last element of the list
 */
void List::RemoveLast()
{
    if(this->first == nullptr)
    {
        //NE FAIT RIEN
    }
    else
    {
        this->first->RemoveLast();
    }

}

/**
 * TODO
 * @brief List::RemoveFirst removes the first element of the list
 */
void List::RemoveFirst()
{ 
    if(this->first == nullptr)
    {
        //NE FAIT RIEN
    }
    else
    {
        Element* temp = this->first->GetNext();
        this->first->Disconnect();
        delete this->first;
        this->first = temp;
    }
}
