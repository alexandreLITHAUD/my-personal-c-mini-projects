#include <iostream>
#include <cstring>
#include "list.h"
using namespace std;

int main()
{
    {
    cout << "TP01 - test 1" << endl;
    List l;

    l.AddFirst(10);
    l.AddLast(15);
    l.AddLast(20);

    // "TEST" GET
    cout << l.Get(0) << endl;

    cout << l.Size() << endl ; // 3
    l.RemoveLast();
    cout << l.Size() << endl; // 2
    l.RemoveFirst();
    cout << l.Size() << endl; // 1

// "TEST" GET
    cout << l.Get(0) << endl;

// "TEST" FIND
    if(l.Find(15))
    {
        cout << "Trouve !" << endl;
    }
    else
    {
        cout << "Pas Trouve !" << endl;
    }


    }
    {
        cout <<"TP01 - test 2" << endl;
        List l;
        for(int i=0;i<100;i++)
        {
            l.AddLast(i);
            //cout << l.Size() << endl;
        }
        for(int i=0;i<l.Size();i++)
        {
            cout << l.Get(i)<<"-";
        }
        cout << endl;
    }

    cout << "Ceci est un test" << endl;
    return 0;
}
