#ifndef LIST_H
#define LIST_H

#include "element.h"

/**
 * @brief The List class a simple linked list of integers
 */
class List
{
private:
    Element* first;
    List(const List&);
    void operator=(const List&);
public:
    List();
    ~List();
    void AddFirst(int val);
    void AddLast(int val);
    bool Find(int val) const;
    int Size() const;
    int Get(int i) const;
    void RemoveFirst();
    void RemoveLast();
};

#endif // LIST_H
