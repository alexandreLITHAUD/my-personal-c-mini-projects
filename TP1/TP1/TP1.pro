TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        element.cpp \
        list.cpp \
        main.cpp

HEADERS += \
    element.h \
    list.h
