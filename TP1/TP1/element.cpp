#include "element.h"

/**
 * TODO
 * @brief Element::Element init the element
 * @param val the value to store
 */
Element::Element(int val)    
{
    this->value = val;
    this->next = nullptr;

}

/**
 * TODO
 * @brief Element::~Element release the memory
 */
Element::~Element()
{
    delete this->next;
}

/**
 * @brief Element::Connect connect the element to another
 * @param elt the new next element
 * This function DO NOT create any element, it only makes the link
 */
void Element::Connect(Element *elt)
{
    next = elt;
}

/**
 * @brief Element::Disconnect remove the link with the next element
 * The function DO NOT destroy any element, it only break the link
 */
void Element::Disconnect()
{
    next=nullptr;
}

/**
 * TODO
 * @brief Element::AddLast add an element at the end of the chain
 * @param elt the element to add
 */
void Element::AddLast(Element *elt)
{
    if(this->next == nullptr)
    {
        this->next = elt;
    }
    else
    {
        this->GetNext()->AddLast(elt);
    }
}

/**
 * TODO
 * @brief Element::Find search the linked list for a value
 * @param value the value to find
 * @return true if the element OR THE NEXTS own the value
 */
bool Element::Find(int value) const
{
    bool ok=false;
    if(this->GetValue() == value)
    {
        //TROUVE
        ok = true;
    }
    else if(this->GetNext() == nullptr)
    {
        //FIN
        ok = false;
    }
    else
    {
        ok = this->GetNext()->Find(value);
    }
	
    return ok;
}

/**
 * TODO
 * @brief Element::Size Calculates the number of elements in the sublist (the element and the next ones)
 * @return the total number of values in the element & his nexts
 */
int Element::Size() const
{
    int size=0;
    if(this->GetNext() == nullptr)
    {
        size++;
        //FIN
    }
    else
    {
        size++;
        size += this->GetNext()->Size();
    }
	
    return size;
}

/**
 * TODO
 * @brief Element::RemoveLast remove the element at the end of the chain, after the present one
 * The function destroy the object after removing it from the list
 */
void Element::RemoveLast()
{
    if(this->GetNext()->next == nullptr)
    {
        delete this->GetNext();
        this->Disconnect();
    }
    else
    {
        this->GetNext()->RemoveLast();
    }
}
