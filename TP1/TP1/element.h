#ifndef ELEMENT_H
#define ELEMENT_H

/**
 * @brief The Element class represents an element of a linked list
 */
class Element
{
private:
    int value;
    Element* next;
    void operator=(const Element&); // to prevent copy
    Element(const Element&); // to prevent copy
public:
    Element(int val);
    ~Element();
    Element* GetNext() const {return next;}
    int GetValue() const {return value;}
    void Connect(Element* elt);
    void Disconnect();
    void AddLast(Element* elt);
    bool Find(int value) const;
    int Size() const;
    void RemoveLast();
};

#endif // ELEMENT_H
