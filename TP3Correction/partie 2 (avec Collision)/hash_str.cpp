#include "hash_str.h"
#include <list>
#include <algorithm>

/**
 * @brief fonction de hachage de la Classe
 * @param la chaine de caractère à "hacher"
 * @return la chaine de caracères "haché"
 */
int hash_str::hash(std::string s)const
{
    int value=0;
    for(char c:s)
    {
        value= value*5 + (int)c;
    }
    return value % this->table.size();
}

/**
 * @brief Constructeur de la Classe hash_str
 * qui definie la taille du tableau a un paremetre size
 * @param la taille souhaiter du tableaux
 */
hash_str::hash_str(int size)
{
    for(int i=0;i<size;i++){
        std::list<std::string> l;
        this->table.push_back(l);
    }
}

/**
 * @brief fonction qui permet de rajouter une chaine de caratères
 * dans le tableau
 * @param la chaine a ajouter au tableau
 */
void hash_str::add(std::string s)
{
    int i = this->hash(s);
    this->table[i].push_back(s);
}

/**
 * @brief fonction qui recherche si une chaine de
 * caractères est dans la tableau
 * @param la chaine de caractères à chercher
 * @return True si la valeur est presente False sinon
 */
bool hash_str::contains(std::string s) const
{
    bool res = false;
    int i = this->hash(s);

    auto temp2 = std::find(table[i].begin(),table[i].end(),s);
    if(temp2 != table[i].end()){
        res = true;
    }

    return res;
}

/**
 * @brief fonction qui permet de supprimer une chaine de caratères
 * dans le tableau
 * @param la chaine a supprimer du tableau
 */
void hash_str::remove(std::string s)
{
    this->table[this->hash(s)].remove(s);
}
