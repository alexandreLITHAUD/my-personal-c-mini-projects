#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include "hash_str.h"
#include "chrono.h"

using namespace std;

int main()
{

iut::Chrono chronometre;

hash_str table(10000);
std::vector<std::string> tableau;
std::set<std::string> arbre;
std::ifstream fichier("dico.txt");
std::string mot;

while(fichier){
    fichier>>mot;
    if(!mot.empty())
    {
        table.add(mot);
        tableau.push_back(mot);
        arbre.insert(mot);
    }
}

do{
    cout<<"vous cherchez ?";
    cin>>mot;
    chronometre.start();
    if(table.contains(mot))
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " hash" << endl;
        //cout<<"il est dans le dico"<<endl;
        chronometre.start();
    }
    else
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " hash" << endl;
        //cout<<"il n'est pas dans le dico"<<endl;
        chronometre.start();
    }

    auto it = std::find(tableau.begin(),tableau.end(),mot);
    if(it != tableau.end())
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " tableau" << endl;
        //cout<<"il est dans le dico"<<endl;
        chronometre.start();
    }
    else
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " tableau" << endl;
        //cout<<"il n'est pas dans le dico"<<endl;
        chronometre.start();
    }

    auto it2 = arbre.find(mot);
    if(it2 != arbre.end())
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " arbre" << endl;
        //cout<<"il est dans le dico"<<endl;
    }
    else
    {
        chronometre.stop();
        cout << chronometre.totalTime() << " arbre" << endl;
        //cout<<"il n'est pas dans le dico"<<endl;
    }

}while(mot!="rien");


}
