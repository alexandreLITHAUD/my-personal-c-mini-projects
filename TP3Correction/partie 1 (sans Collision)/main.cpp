#include <iostream>
#include "hash_str.h"

using namespace std;

int main()
{
    hash_str table;
    table.add("voiture");
    table.add("camion");
    table.add("vélo");
    if(table.contains("voiture"))
    {
        cout<<"ok"<<endl;
    }
    else
    {
        cout<<"nok"<<endl;
    }
    if(!table.contains("avion"))
    {
        cout<<"ok"<<endl;
    }
    else
    {
        cout<<"nok"<<endl;
    }
}
