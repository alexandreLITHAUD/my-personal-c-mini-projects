#include "hash_str.h"

/**
 * @brief fonction de hachage de la Classe
 * @param la chaine de caractère à "hacher"
 * @return la chaine de caracères "haché"
 */
int hash_str::hash(std::string s)const
{
    int value=0;
    for(char c:s)
    {
        value= value*5 + (int)c;
    }
    return value % this->table.size();
}

/**
 * @brief Constructeur de la Classe hash_str
 * qui definie la taille du tableau a un paremetre size
 * @param la taille souhaiter du tableaux
 */
hash_str::hash_str(int size)
{
    for(int i=0;i<size;i++){
        this->table.push_back("");
    }
}

/**
 * @brief fonction qui permet de rajouter une chaine de caratères
 * dans le tableau
 * @param la chaine a ajouter au tableau
 */
void hash_str::add(std::string s)
{
    int temp = this->hash(s);
    this->table.at(temp) = s;
}

/**
 * @brief fonction qui recherche si une chaine de
 * caractères est dans la tableau
 * @param la chaine de caractères à chercher
 * @return True si la valeur est presente False sinon
 */
bool hash_str::contains(std::string s) const
{
    bool res = false;
    int temp = this->hash(s);

    if(!this->table.at(temp).empty()){
        res = true;
    }

    return res;
}

/**
 * @brief fonction qui permet de supprimer une chaine de caratères
 * dans le tableau
 * @param la chaine a supprimer du tableau
 */
void hash_str::remove(std::string s)
{
    this->table.at(this->hash(s)) = "";
}
